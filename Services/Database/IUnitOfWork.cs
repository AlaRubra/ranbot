using RanBot.Services.Database.Repositories;
using System;
using System.Threading.Tasks;

namespace RanBot.Services.Database
{
    public interface IUnitOfWork : IDisposable
    {
        RanContext _context { get; }

        IGuildConfigRepository GuildConfigs { get; }
        IBotConfigRepository BotConfig { get; }
        IRanUserRepository RanUsers { get; }
        IUserCountingStatsRepository UserCountingStats { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}