namespace RanBot.Services.Database.Models
{
    public class RanUser : DbEntity
    {
        public ulong UserId { get; set; }
        public string Username { get; set; }
        public string Discriminator { get; set; }
        public int TotalCounted { get; set; }

        public override bool Equals(object obj)
        {
            return obj is RanUser du
                ? du.UserId == UserId
                : false;
        }

        public override int GetHashCode()
        {
            return UserId.GetHashCode();
        }

        public override string ToString() =>
            Username + "#" + Discriminator;
    }

}