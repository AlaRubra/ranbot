using DSharpPlus.Entities;
using RanBot.Services.Database.Models;

namespace RanBot.Services.Database.Repositories
{
    public interface IRanUserRepository : IRepository<RanUser>
    {
        RanUser GetOrCreate(ulong userId, string username, string discrim);
        RanUser GetOrCreate(DiscordUser original);
        RanUser[] GetUsersCountLeaderboardFor(int page);
        RanUser GetRanUser(ulong userId);
    }
}
