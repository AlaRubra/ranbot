using DSharpPlus.Entities;
using Microsoft.EntityFrameworkCore;
using RanBot.Services.Database.Models;
using System.Linq;

namespace RanBot.Services.Database.Repositories.Impl
{
    public class RanUserRepository : Repository<RanUser>, IRanUserRepository
    {
        public RanUserRepository(DbContext context) : base(context)
        {
        }

        public RanUser GetOrCreate(ulong userId, string username, string discrim)
        {
            var user = _set
                .FirstOrDefault(u => u.UserId == userId);
            if (user == null)
            {
                _set.Add(new RanUser
                {
                    UserId = userId,
                    Username = username,
                    Discriminator = discrim
                });
                _context.SaveChanges();
                user = _set.First(u => u.UserId == userId);
            }
            else
            {
                user.Username = username;
                user.Discriminator = discrim;
                _context.SaveChanges();
            }
            return user;
        }

        public RanUser GetOrCreate(DiscordUser original)
            => GetOrCreate(original.Id, original.Username, original.Discriminator);

        public RanUser GetRanUser(ulong userId)
        {
            var user = _set
                .FirstOrDefault(u => u.UserId == userId);
            return user;
        }


        public int GetUserGlobalRank(ulong id)
        {
            return _set.AsQueryable()
                .Where(x => x.TotalCounted > (_set
                    .AsQueryable()
                    .Where(y => y.UserId == id)
                    .Select(y => y.TotalCounted)
                    .FirstOrDefault()))
                .Count() + 1;
        }

        public RanUser[] GetUsersCountLeaderboardFor(int page)
        {
            return _set.AsQueryable()
                .OrderByDescending(x => x.TotalCounted)
                .Skip(page * 9)
                .Take(9)
                .AsEnumerable()
                .ToArray();
        }
    }
}
