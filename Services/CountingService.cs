using DSharpPlus;
using DSharpPlus.EventArgs;
using System;
using System.Threading.Tasks;

namespace RanBot.Services
{
    public class CountingService
    {
        private readonly DiscordClient _client;
        private readonly IServiceProvider _provider;
        private readonly DbService _db;

        public CountingService(DiscordClient client, IServiceProvider provider, DbService db)
        {
            _client = client;
            _provider = provider;
            _db = db;
            _client.MessageCreated += HandleCountAsync;
            _client.MessageDeleted += HandleDeletedCountAsync;
            _client.MessageUpdated += HandleEditedCountAsync;
            _client.MessagesBulkDeleted += HandleBulkDeleteAsync;
        }

        private async Task HandleCountAsync(DiscordClient client, MessageCreateEventArgs eventArgs)
        {
            // check if message is a system message. If it is, bail.

            var message = eventArgs.Message;
            if (message.MessageType != MessageType.Default || message.Author.IsCurrent)
                return;
            // check if message is in a guild. If it isn't, bail.
            var channel = message.Channel;
            if (channel.Guild == null)
                return;
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                //bail if not in counting channel
                if (channel.Id != config.CountingChannelId)
                    return;
                if (message.Author.IsBot || message.Author.Id == config.LastCountedUser)
                {
                    //if bot message or last user to count, delete it
                    await message.DeleteAsync();
                    return;
                }
                var messageAsArray = message.Content.Split(' ');
                if ((messageAsArray.Length != 1 || message.Attachments.Count != 0) && config.AllowMessages == false)
                {
                    //if message contains additionalmessage and messages not allowed, delete the message.
                    await message.DeleteAsync();
                    return;
                }

                var possibleNumber = messageAsArray[0];
                int increment = 1;

                if (config.CountDown)
                    increment = -1;

                if ((config.CurrentCount + increment != 0 && possibleNumber.Substring(0, 1) == "0") || (config.CurrentCount + increment < 0 && possibleNumber.Substring(1, 1) == "0"))
                {
                    await message.DeleteAsync();
                    return;
                }

                var validNumber = int.TryParse(possibleNumber, out var number);
                if (!validNumber || number != config.CurrentCount + increment)
                {
                    await message.DeleteAsync();
                    return;
                }
                config.LastCountedUser = message.Author.Id;
                config.LastCountMessage = message.Id;
                config.CurrentCount += increment;

                var user = uow.RanUsers.GetOrCreate(message.Author);
                user.TotalCounted++;
                var userStats = uow.UserCountingStats.GetOrCreateUser(channel.Guild.Id, message.Author.Id);
                userStats.Counted++;

                uow.SaveChanges();
            }
        }

        private async Task HandleDeletedCountAsync(DiscordClient client, MessageDeleteEventArgs eventArgs)
        {
            var message = eventArgs.Message;
            var channel = message.Channel;
            if (channel.Guild == null)
                return;
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                if (message.Id == config.LastCountMessage)
                {
                    var user = uow.RanUsers.GetRanUser(config.LastCountedUser);
                    var botMessage = await message.Channel.SendMessageAsync($"{config.CurrentCount} (**{user}**)");
                    config.LastCountMessage = botMessage.Id;
                    uow.SaveChanges();
                }
            }
        }

        private async Task HandleEditedCountAsync(DiscordClient client, MessageUpdateEventArgs eventArgs)
        {
            var message = eventArgs.Message;
            if (!message.IsEdited)
                return;
            var channel = message.Channel;
            if (channel.Guild == null)
                return;
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                if (message.Id == config.LastCountMessage)
                {
                    await message.DeleteAsync();
                }
            }
        }

        private async Task HandleBulkDeleteAsync(DiscordClient client, MessageBulkDeleteEventArgs eventArgs)
        {
            if (eventArgs.Guild == null)
                return;
            var channel = eventArgs.Channel;
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                foreach (var message in eventArgs.Messages)
                {
                    if (message.Id == config.LastCountMessage)
                    {
                        var user = uow.RanUsers.GetRanUser(config.LastCountedUser);
                        var botMessage = await message.Channel.SendMessageAsync($"{config.CurrentCount} (**{user}**)");
                        config.LastCountMessage = botMessage.Id;
                        uow.SaveChanges();
                    }
                }
            }
        }
    }
}