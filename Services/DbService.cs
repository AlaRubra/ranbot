using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using RanBot.Services.Database;
using System;
using System.IO;
using System.Linq;

namespace RanBot.Services
{
    public class DbService
    {
        private readonly DbContextOptions<RanContext> options;
        private readonly DbContextOptions<RanContext> migrateOptions;

        public DbService(IBotCredentials creds)
        {
            var builder = new SqliteConnectionStringBuilder(creds.Db.ConnectionString);
            builder.DataSource = Path.Combine(AppContext.BaseDirectory, builder.DataSource);

            var optionsBuilder = new DbContextOptionsBuilder<RanContext>();
            optionsBuilder.UseSqlite(builder.ToString());
            options = optionsBuilder.Options;

            optionsBuilder = new DbContextOptionsBuilder<RanContext>();
            optionsBuilder.UseSqlite(builder.ToString());
            migrateOptions = optionsBuilder.Options;
        }

        public void Setup()
        {
            using (var context = new RanContext(options))
            {
                if (context.Database.GetPendingMigrations().Any())
                {
                    var mContext = new RanContext(migrateOptions);
                    mContext.Database.Migrate();
                    mContext.SaveChanges();
                    mContext.Dispose();
                }
                context.Database.ExecuteSqlRaw("PRAGMA journal_mode=WAL");
                context.EnsureSeedData();
                context.SaveChanges();
            }
        }

        private RanContext GetDbContextInternal()
        {
            var context = new RanContext(options);
            context.Database.SetCommandTimeout(60);
            var conn = context.Database.GetDbConnection();
            conn.Open();
            using (var com = conn.CreateCommand())
            {
                com.CommandText = "PRAGMA journal_mode=WAL; PRAGMA synchronous=OFF";
                com.ExecuteNonQuery();
            }
            return context;
        }

        public IUnitOfWork GetDbContext() => new UnitOfWork(GetDbContextInternal());
    }
}
