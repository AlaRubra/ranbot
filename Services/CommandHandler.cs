﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using RanBot.Common;
using RanBot.Extensions;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace RanBot.Services
{
    public class CommandHandler
    {
        private DiscordClient _client;
        private CommandsNextExtension _commands;
        private IServiceProvider _services;
        private DbService _db;
        public CommandHandler(DiscordClient client, IServiceProvider services, DbService db)
        {
            _client = client;
            _services = services;
            _commands = _client.UseCommandsNext(new CommandsNextConfiguration()
            {
                UseDefaultCommandHandler = false,
                Services = _services
            });

            _commands.SetHelpFormatter<RanHelpFormatter>();

            _db = db;

            _client.MessageCreated += HandleCommand;

            _commands.RegisterCommands(Assembly.GetExecutingAssembly());
        }

        private Task HandleCommand(DiscordClient client, MessageCreateEventArgs eventArgs)
        {
            var commandsNext = client.GetCommandsNext();
            var message = eventArgs.Message;

            var prefix = message.GetPrefix();

            var cmdStart = message.GetStringPrefixLength(prefix);
            if (cmdStart == -1)
            {
                cmdStart = message.GetMentionPrefixLength(_client.CurrentUser);
                prefix = _client.CurrentUser.Mention;
            }

            if (cmdStart == -1) return Task.CompletedTask;

            if (message.Channel.Guild != null)
            {
                using (var uow = _db.GetDbContext())
                {
                    var config = uow.GuildConfigs.ForId((ulong)message.Channel.GuildId);
                    if (config.CountingChannelId == message.ChannelId) return Task.CompletedTask;
                }
            }

            var cmdString = message.Content.Substring(cmdStart);

            var command = commandsNext.FindCommand(cmdString, out var args);
            if (command == null) return Task.CompletedTask;

            var ctx = commandsNext.CreateContext(message, prefix, command, args);
            Task.Run(async () => await commandsNext.ExecuteCommandAsync(ctx));

            return Task.CompletedTask;
        }
    }
}
