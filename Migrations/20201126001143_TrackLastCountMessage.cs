﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RanBot.Migrations
{
    public partial class TrackLastCountMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "LastCountMessage",
                table: "GuildConfigs",
                nullable: false,
                defaultValue: 0ul);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastCountMessage",
                table: "GuildConfigs");
        }
    }
}
