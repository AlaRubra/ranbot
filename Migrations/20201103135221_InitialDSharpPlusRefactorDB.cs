﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace RanBot.Migrations
{
    public partial class InitialDSharpPlusRefactorDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BotConfig",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    OkColor = table.Column<string>(nullable: true, defaultValue: "009113"),
                    ErrorColor = table.Column<string>(nullable: true, defaultValue: "910000"),
                    DefaultPrefix = table.Column<string>(nullable: true),
                    DefaultLanguage = table.Column<string>(nullable: true, defaultValue: "en-US")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotConfig", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GuildConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    GuildId = table.Column<ulong>(nullable: false),
                    Prefix = table.Column<string>(nullable: true),
                    CountingChannelId = table.Column<ulong>(nullable: false),
                    LastCountedUser = table.Column<ulong>(nullable: false),
                    OkColor = table.Column<string>(nullable: true),
                    ErrorColor = table.Column<string>(nullable: true),
                    CountDown = table.Column<bool>(nullable: false),
                    AllowMessages = table.Column<bool>(nullable: false),
                    DefaultLanguage = table.Column<string>(nullable: true),
                    CurrentCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuildConfigs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RanUser",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<ulong>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: true),
                    TotalCounted = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RanUser", x => x.Id);
                    table.UniqueConstraint("AK_RanUser_UserId", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "UserCountingStats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<ulong>(nullable: false),
                    GuildId = table.Column<ulong>(nullable: false),
                    Counted = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCountingStats", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GuildConfigs_GuildId",
                table: "GuildConfigs",
                column: "GuildId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserCountingStats_GuildId",
                table: "UserCountingStats",
                column: "GuildId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCountingStats_UserId",
                table: "UserCountingStats",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCountingStats_UserId_GuildId",
                table: "UserCountingStats",
                columns: new[] { "UserId", "GuildId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BotConfig");

            migrationBuilder.DropTable(
                name: "GuildConfigs");

            migrationBuilder.DropTable(
                name: "RanUser");

            migrationBuilder.DropTable(
                name: "UserCountingStats");
        }
    }
}
