RanBot

A work in progress Discord counting bot written in C# using Discord.Net

Selfhosting this bot is not recommended at this point, as it's still in early development, but eventually, selfhosting guides will be created.

Our Support server can be found here: https://discord.gg/sbZFUdw

Database code largely based on NadekoBot, by Kwoth https://gitlab.com/Kwoth/nadekobot. License included in /vendored licenses

//TODO:
//Refactor db code to no longer be copied from Nadeko. Remove repository pattern.