using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.CommandsNext.Entities;
using RanBot.Extensions;

namespace RanBot.Common
{
    public class RanHelpFormatter : DefaultHelpFormatter
    {
        public RanHelpFormatter(CommandContext ctx) : base(ctx) { }

        public override CommandHelpMessage Build()
        {
            EmbedBuilder.WithOkColor(base.Context);
            return base.Build();
        }
    }
}