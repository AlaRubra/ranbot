using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using RanBot.Services;
using RanBot.Services.Impl;
using System;
using System.Globalization;

namespace RanBot.Extensions
{
    public static class Extensions
    {
        private static readonly IBotCredentials _creds = new BotCredentials();
        private static readonly DbService _db = new DbService(_creds);
        public static string GetPrefix(this DiscordMessage message)
        {
            string prefix;
            using (var uow = _db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate();
                if (message.Channel.Guild != null)
                {
                    var config = uow.GuildConfigs.ForId((ulong)message.Channel.GuildId);
                    if (config.Prefix == null)
                        prefix = bc.DefaultPrefix;
                    else
                        prefix = config.Prefix;
                }
                else
                    prefix = bc.DefaultPrefix;
            }
            return prefix;
        }

        public static DiscordEmbedBuilder WithOkColor(this DiscordEmbedBuilder eb, CommandContext ctx)
        {
            using (var uow = _db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate();
                var defOkColor = new DiscordColor(Convert.ToInt32(bc.OkColor, 16));
                if (ctx.Guild != null)
                {
                    var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                    if (config.OkColor == null)
                        eb.WithColor(defOkColor);
                    else
                    {
                        var guildOkColor = new DiscordColor(Convert.ToInt32(config.OkColor, 16));
                        eb.WithColor(guildOkColor);
                    }
                }
                else
                    eb.WithColor(defOkColor);
            }
            return eb;
        }

        public static DiscordEmbedBuilder WithErrorColor(this DiscordEmbedBuilder eb, CommandContext ctx)
        {
            using (var uow = _db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate();
                var defErrorColor = new DiscordColor(Convert.ToInt32(bc.ErrorColor, 16));
                if (ctx.Guild != null)
                {
                    var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                    if (config.ErrorColor == null)
                        eb.WithColor(defErrorColor);
                    else
                    {
                        var guildErrorColor = new DiscordColor(Convert.ToInt32(config.ErrorColor, 16));
                        eb.WithColor(guildErrorColor);
                    }
                }
                else
                    eb.WithColor(defErrorColor);
            }
            return eb;
        }

        public static bool ValidateHexColor(this string color)
        {
            return (color.Length == 6 && int.TryParse(color, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out int hexParseChecker) && hexParseChecker < 16777215);
        }
    }
}