﻿namespace RanBot
{
    class Program
    {
        static void Main(string[] args)
        {
            new RanBot().Ran().ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}
