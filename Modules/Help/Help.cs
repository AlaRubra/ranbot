using Discord;
using Discord.Commands;
using System.Threading.Tasks;
using RanBot.Services.Impl;
using RanBot.Common.Attributes;
using RanBot.Extensions;

namespace RanBot.Modules
{
    public class HelpModule : ModuleBase<SocketCommandContext>
    {
        private readonly CommandList _commandList = new CommandList("en-US");

        [RanCommand, Aliases, Use, Description]
        [Priority (0)]
        public async Task Help()
        {
            var embed = new EmbedBuilder()
                .WithTitle("RanBot Help")
                .WithDescription("Hi, I'm Ran, a Discord counting bot by **AlaRubra#1234**\n\nJoin my [support server](https://discord.gg/sbZFUdw).")
                .WithOkColor(Context.Message);
            await Context.Channel.SendMessageAsync("", embed: embed.Build());
        }

        [RanCommand, Aliases, Use, Description]
        [Priority (1)]
        public async Task Help(string cmd)
        {
            var command = _commandList.LoadCommand(cmd);
            var embed = new EmbedBuilder()
                .WithDescription($"`{command.Cmd}`")
                .AddField(efb => efb.WithName("Description").WithValue(command.Desc).WithIsInline(false))
                .AddField(efb => efb.WithName("Use").WithValue(command.Use[0]).WithIsInline(false))
                .WithOkColor(Context.Message);
            await Context.Channel.SendMessageAsync("", embed: embed.Build());
        }
    }
}