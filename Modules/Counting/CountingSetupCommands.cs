﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using RanBot.Extensions;
using RanBot.Modules.Services;
using RanBot.Services;
using RanBot.Services.Impl;
using System.Threading.Tasks;

namespace RanBot.Modules
{
    public partial class CountingModule : Base​Command​Module
    {
        //this should really be handled through dependency injection
        private static readonly IBotCredentials _creds = new BotCredentials();
        private static readonly DbService _db = new DbService(_creds);

        [Command, Aliases, Description("Set the counting channel for the current server.")]
        [RequireUserPermissions(Permissions.ManageChannels | Permissions.ManageMessages)]
        public async Task CountingChannel(CommandContext ctx, DiscordChannel channel = null, int count = 0)
        {
            if (channel == null)
                channel = await ctx.Guild.CreateTextChannelAsync("counting", ctx.Channel.Parent);
            if (channel.Guild == ctx.Guild)
                await CountingCommandsService.SetCountingChannel(channel, ctx, count);
            else
                return;
        }
        [Command, Aliases, Description("change the counting channel to countdown from a specific number.")]
        [RequireUserPermissions(Permissions.ManageMessages)]
        public async Task CountDownFrom(CommandContext ctx, int number)
        {
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                config.CountDown = true;
                config.CurrentCount = number + 1;
                uow.SaveChanges();
            }
            var embed = new DiscordEmbedBuilder()
                .WithDescription($"You got it. Counting down from {number}")
                .WithOkColor(ctx);
            await ctx.Channel.SendMessageAsync("", embed: embed.Build());
        }

        [Command, Aliases, Description("Toggles whether or not a message is allowed in the counting")]
        [RequireUserPermissions(Permissions.ManageMessages)]
        public async Task AllowMessages(CommandContext ctx)
        {
            string setting;
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                config.AllowMessages = !config.AllowMessages;
                if (config.AllowMessages)
                    setting = "true";
                else
                    setting = "false";
                uow.SaveChanges();
            }
            var embed = new DiscordEmbedBuilder()
                .WithDescription($"Allow messages setting updated to {setting}.")
                .WithOkColor(ctx);
            await ctx.Channel.SendMessageAsync("", embed: embed.Build());
        }
    }
}