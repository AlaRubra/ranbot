using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using RanBot.Extensions;
using RanBot.Modules.Services;
using System.Threading.Tasks;

namespace RanBot.Modules
{
    public partial class CountingModule : Base​Command​Module
    {
        [Command, Aliases, Description("Show the leaderborad for the current server.")]
        [RequireGuild]
        public async Task LeaderBoard(CommandContext ctx)
        {
            var pages = CountingCommandsService.GetLeaderdoardPages(ctx);
            await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
        }

        [Command, Aliases, Description("Show the global counting leaderboard.")]
        public async Task GlobalLeaderBoard(CommandContext ctx)
        {
            var pages = CountingCommandsService.GetGlobalPages(ctx);
            await ctx.Channel.SendPaginatedMessageAsync(ctx.User, pages);
        }

        [Command, Aliases, Description("Completely delete all data the bot has stored regarding you.")]
        public async Task ForgetMe(CommandContext ctx)
        {
            var confirmEmoji = DiscordEmoji.FromName(ctx.Client, ":white_check_mark:");
            var cancelEmoji = DiscordEmoji.FromName(ctx.Client, ":x:");
            var embed = new DiscordEmbedBuilder()
                .WithOkColor(ctx)
                .WithDescription($"Are you sure, {ctx.User.Username}? This will completely reset your stats on the global leaderboard, and all server leaderboards.\n\nReact with {confirmEmoji} to confirm or {cancelEmoji} to cancel.");

            var message = await ctx.Channel.SendMessageAsync("", embed: embed.Build());

            await message.CreateReactionAsync(confirmEmoji);
            await message.CreateReactionAsync(cancelEmoji);
            var confirm = await message.WaitForReactionAsync(ctx.User);
            if (confirm.Result.Emoji == confirmEmoji)
            {
                using (var uow = _db.GetDbContext())
                {
                    var user = uow.RanUsers.GetOrCreate(ctx.User);
                    uow.RanUsers.Remove(user.Id);
                    uow.UserCountingStats.ResetUserCounting(user.UserId);
                    uow.SaveChanges();
                }
                embed = new DiscordEmbedBuilder()
                    .WithOkColor(ctx)
                    .WithDescription("Your data has been deleted.");
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else if (confirm.Result.Emoji != confirmEmoji)
            {
                embed = new DiscordEmbedBuilder()
                    .WithErrorColor(ctx)
                    .WithDescription("Canceled. Your data has not been affected.");
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else if (confirm.TimedOut)
            {
                embed = new DiscordEmbedBuilder()
                    .WithErrorColor(ctx)
                    .WithDescription("Timed out. Your data has not been affected.");
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
            await message.DeleteAllReactionsAsync();
        }
    }
}