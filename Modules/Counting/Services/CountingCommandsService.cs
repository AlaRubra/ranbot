using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using RanBot.Extensions;
using RanBot.Services;
using RanBot.Services.Database.Models;
using RanBot.Services.Impl;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RanBot.Modules.Services
{
    public static class CountingCommandsService
    {
        //this should really be handled through dependency injection, and this class should probably not be static.
        private static readonly IBotCredentials _creds = new BotCredentials();
        private static readonly DbService _db = new DbService(_creds);

        public static async Task SetCountingChannel(DiscordChannel channel, CommandContext ctx, int count)
        {
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(channel.Guild.Id);
                config.CountingChannelId = channel.Id;
                config.CurrentCount = count;
                config.CountDown = false;
                uow.SaveChanges();
            }
            await channel.ModifyAsync(x => { x.PerUserRateLimit = 2; });
            var embed = new DiscordEmbedBuilder()
                .WithDescription($"You got it. Counting channel set to {channel.Mention}")
                .WithOkColor(ctx);
            await ctx.Channel.SendMessageAsync("", embed: embed.Build());
        }

        public static List<Page> GetLeaderdoardPages(CommandContext ctx)
        {
            var pages = new List<Page>();
            using (var uow = _db.GetDbContext())
            {
                var stats = uow.UserCountingStats.GetTopUserCountings(ctx.Guild.Id, 1000);
                var numberOfPages = stats.Count / 9;
                if (stats.Count % 9 != 0)
                    numberOfPages++;
                for (int i = 0; i < numberOfPages; i++)
                {
                    var embed = new DiscordEmbedBuilder()
                        .WithTitle("Counting Leaderboard")
                        .WithOkColor(ctx);
                    var usersOnPage = uow.UserCountingStats.GetUsersFor(ctx.Guild.Id, i);
                    var j = 1;
                    foreach (var user in usersOnPage)
                    {
                        var username = uow.RanUsers.GetRanUser(user.UserId).ToString();
                        embed.AddField($"#{i * 9 + j} {username}", $"Total Counted:{user.Counted}", true);
                        j++;
                    }
                    embed.WithFooter($"Page: {i + 1} / {numberOfPages}");
                    pages.Add(new Page("", embed));
                }
            }
            return pages;
        }

        public static List<Page> GetGlobalPages(CommandContext ctx)
        {
            var pages = new List<Page>();
            using (var uow = _db.GetDbContext())
            {
                var stats = uow.RanUsers.GetAll() as List<RanUser>;
                var numberOfPages = stats.Count / 9;
                if (stats.Count % 9 != 0)
                    numberOfPages++;
                for (int i = 0; i < numberOfPages; i++)
                {
                    var embed = new DiscordEmbedBuilder()
                        .WithTitle("Counting Global Leaderboard")
                        .WithOkColor(ctx);
                    var usersOnPage = uow.RanUsers.GetUsersCountLeaderboardFor(i);
                    var j = 1;
                    foreach (var user in usersOnPage)
                    {
                        embed.AddField($"#{i * 9 + j} {user}", $"Total Counted:{user.TotalCounted}", true);
                        j++;
                    }
                    embed.WithFooter($"Page: {i + 1} / {numberOfPages}");
                    pages.Add(new Page("", embed));
                }
            }
            return pages;
        }
    }
}