﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using RanBot.Extensions;
using System.Threading.Tasks;

namespace RanBot.Modules
{
    public partial class SelfModule : Base​Command​Module
    {
        [Command, Aliases, Description("Set the guild prefix. Provide parameters to see the current prefix. **Setting the prefix requires the Administrator permission.**")]
        [Priority(1)]
        public async Task Prefix(CommandContext ctx)
        {
            var currentPrefix = ctx.Message.GetPrefix();
            var embed = new DiscordEmbedBuilder()
                .WithDescription($"Current prefix is {currentPrefix}.")
                .WithOkColor(ctx);
            await ctx.Channel.SendMessageAsync("", embed: embed.Build());
        }

        [Command]
        [Priority(0)]
        public async Task Prefix(CommandContext ctx, string prefix)
        {
            var embed = new DiscordEmbedBuilder();
            if (!ctx.Member.PermissionsIn(ctx.Channel).HasPermission(Permissions.Administrator))
            {
                var currentPrefix = ctx.Message.GetPrefix();
                embed.WithDescription($"You do not have permission to set the prefix. Current prefix is {currentPrefix}.")
                    .WithErrorColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
                return;
            }
            using (var uow = _db.GetDbContext())
            {
                var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                config.Prefix = prefix;
                uow.SaveChanges();
            }
            embed.WithDescription($"You got it. Prefix set to {prefix}")
                 .WithOkColor(ctx);
            await ctx.Channel.SendMessageAsync("", embed: embed.Build());
        }

        [Command, Aliases, Description("Set the default prefix.")]
        [RequireOwner]
        public async Task DefPrefix(CommandContext ctx, string prefix)
        {
            using (var uow = _db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate();
                bc.DefaultPrefix = prefix;
                uow.SaveChanges();
            }
            var embed = new DiscordEmbedBuilder()
                .WithDescription($"Default prefix changed to {prefix}")
                .WithOkColor(ctx);
            await ctx.Channel.SendMessageAsync("", embed: embed.Build());
        }
    }
}