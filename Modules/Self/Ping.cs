using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using RanBot.Services;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RanBot.Modules
{
    public partial class SelfModule : Base​Command​Module
    {
        public IBotCredentials _creds { private get; set; }
        public DbService _db { private get; set; }

        [Command, Aliases, Description("A basic ping command to check for latency issues.")]
        public async Task Ping(CommandContext ctx)
        {
            var sw = Stopwatch.StartNew();
            var msg = await ctx.RespondAsync("🏓").ConfigureAwait(false);
            sw.Stop();

            await msg.DeleteAsync();

            await ctx.RespondAsync($"🏓 {(int)sw.Elapsed.TotalMilliseconds}ms");
        }
    }
}