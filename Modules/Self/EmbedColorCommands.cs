﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using RanBot.Extensions;
using System.Threading.Tasks;

namespace RanBot.Modules
{
    public partial class SelfModule : Base​Command​Module
    {

        [Command, Aliases, Description("Sets the OkColor for the current server. This color will be used for all successful command confirmations.")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task OkColor(CommandContext ctx, string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                    config.OkColor = color;
                    uow.SaveChanges();
                }
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"You got it. OkColor set to {color}")
                    .WithOkColor(ctx);
                await ctx.RespondAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(ctx);
                await ctx.RespondAsync("", embed: embed.Build());
            }
        }

        [Command, Aliases, Description("Sets the ErrorColor for the current server. This color will be used for all error responses.")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task ErrorColor(CommandContext ctx, string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var config = uow.GuildConfigs.ForId(ctx.Guild.Id);
                    config.ErrorColor = color;
                    uow.SaveChanges();
                }
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"You got it. Error Color set to {color}")
                    .WithOkColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
        }

        [Command, Aliases, Description("Sets the default OkColor. This will be the default color for all successful command confirmations.")]
        [RequireOwner]
        public async Task DefOkColor(CommandContext ctx, string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var bc = uow.BotConfig.GetOrCreate();
                    bc.OkColor = color;
                    uow.SaveChanges();
                }
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"Default OkColor changed to {color}")
                    .WithOkColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
        }

        [Command, Aliases, Description("Sets the default ErrorColor. This will be the default color for all error responses.")]
        [RequireOwner]
        public async Task DefErrorColor(CommandContext ctx, string color)
        {
            if (color.ValidateHexColor())
            {
                using (var uow = _db.GetDbContext())
                {
                    var bc = uow.BotConfig.GetOrCreate();
                    bc.ErrorColor = color;
                    uow.SaveChanges();
                }
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"Default ErrorColor changed to {color}")
                    .WithOkColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
            else
            {
                var embed = new DiscordEmbedBuilder()
                    .WithDescription($"Please enter a valid color hex code.")
                    .WithErrorColor(ctx);
                await ctx.Channel.SendMessageAsync("", embed: embed.Build());
            }
        }
    }
}