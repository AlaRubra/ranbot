﻿using DSharpPlus;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RanBot.Services;
using RanBot.Services.Impl;
using System;
using System.Threading.Tasks;

namespace RanBot
{
    class RanBot
    {
        static DiscordClient _client;
        private IBotCredentials _creds;
        private DbService _db;
        private IServiceProvider _services;
        private CommandHandler _commandHandler;
        private CountingService _countingService;
        public async Task Ran()
        {
            _creds = new BotCredentials();
            _db = new DbService(_creds);
            _db.Setup();
            _client = new DiscordClient(new DiscordConfiguration
            {
                Token = _creds.Token,
                TokenType = TokenType.Bot,
                MinimumLogLevel = LogLevel.Debug
            });
            _client.UseInteractivity();
            _services = RanServiceProvider();

            _commandHandler = new CommandHandler(_client, _services, _db);
            _countingService = new CountingService(_client, _services, _db);

            await _client.ConnectAsync();

            await Task.Delay(-1);
        }

        public IServiceProvider RanServiceProvider() => new ServiceCollection()
            .AddSingleton(_db)
            .AddSingleton(_client)
            .BuildServiceProvider();

    }
}
